import React from 'react';
import TodoClass from './Todo';
import { render } from 'react-dom';

class Title extends React.Component{
  render(){
    return (
    <div>
       <div>
          <h1>to-do ({this.props.todoCount})</h1>
       </div>
    </div>
  );}
}

class TodoForm extends React.Component{
  addTodo(e){
    e.preventDefault();
    this.props.addTodo(this.input.value);
    this.input.value = '';
  }
  render(){
    // Return JSX
    return (
      <form onSubmit={this.addTodo.bind(this)}>
        <input className="form-control col-md-12" ref={(node) => {
          this.input = node;
        }} />
        <br />
      </form>);
  }
}

class Todo extends React.Component{

  removeTodo(){
    //TODO
    this.props.remove(this.props.todo.id)
  }

  render(){
    return (<a href="#" className="list-group-item" onClick={this.removeTodo.bind(this)}>{this.props.todo.title}</a>);
  }
}

class TodoList extends React.Component{
  render(){
    const todoList = this.props.todos.map((todo, index) => {
      return (<Todo todo={todo} key={index} remove={this.props.remove}/>)
    });
     return (
       <div className="list-group" style={{marginTop:'30px'}}>
        {todoList}
       </div>);
  }
}

// Contaner Component
// Todo Id

class TodoApp extends React.Component{
  constructor(props){
    // Pass props to parent class
    super(props);
    // Set initial state
    this.state = {
      todos: []
    }
  }
  handleRemove(id){
    const todos = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({todos});
  }
  addTodo(todo){
    const todos = [...this.state.todos, new TodoClass(todo)];
    this.setState({todos});
  }
  render(){
   // Render JSX
    return (
      <div>
        <Title todoCount={this.state.todos.length}/>
        <TodoForm addTodo={this.addTodo.bind(this)}/>
        <TodoList
          todos={this.state.todos}
          remove={this.handleRemove.bind(this)}
        />
      </div>
    );
  }
}

render(<TodoApp />, document.getElementById('container'));
